@extends('layouts.app')


@section('tabName')
	List creation
@endsection

@section('content')
	<form class = "col-3 bg-secondary p-5 mx-auto" method = "POST" action = '/tasks'>
		<!-- CSRF stands for Cross-Site Request Forgery. It is form of attact where malicious users may send malicious request while pretending to be authorize user. Laravel uses token to detect if form input request have not been tampered with. -->

		@csrf
		<div class = "form-group">
			<label for = "content">Content:</label>
			<textarea class = "form-control" id = "content" name = "content" rows =3></textarea>
		</div>

		<div class = 'mt-2 '>
			<button class= "btn btn-primary">Create Post</button>
		</div>
	</form>

	@endsection



