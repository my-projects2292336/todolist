@extends("layouts.app")

@section('tabName')
    Newsfeed
@endsection

@section('content')
<div class="container">
    <div class="text-center">
        <h3 class="mb-4">To-Do List</h3>

        @if(session('success'))
            <div class="alert alert-success mt-3">
                {{ session('success') }}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger mt-3">
                {{ session('error') }}
            </div>
        @endif

        <form method="POST" action='/tasks'>
            @csrf
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Add a new task" name="content">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Add Task</button>
                </div>
            </div>
        </form>

        @if(count($tasks) > 0)
            <ul class="list-group col-md-6 mx-auto">
                @foreach($tasks as $task)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <input type="checkbox" name="isActive" value="1" {{$task->isActive ? 'checked' : ''}}>
                        {{$task->body}}

                        <div>
                            @if(Auth::user())
                                <button type="button" class="btn btn-primary btn-sm mx-1" data-bs-toggle="modal" data-bs-target="#updateModal{{$task->id}}">
                                    Update
                                </button>
                            @endif

                            <form action="{{ route('tasks.destroy', ['id' => $task->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm mx-1">Delete</button>
                            </form>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>

@foreach($tasks as $task)
    <div class="modal fade" id="updateModal{{$task->id}}" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Task</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/tasks/{{$task->id}}" method="POST" name="content">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <textarea name="body" class="form-control" rows="3" placeholder="Update your Task">{{$task->body}}</textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" name="update" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection
