<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TaskController;
use App\Models\Task;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/logout', [TaskController::class, 'logout']);

Route::get('/tasks/create',[TaskController::class, 'createTask']);

Route::post('tasks', [TaskController::class, 'saveTask']);
Route::get('/tasks', [TaskController::class, 'showTasks']);

Route::get('tasks/{id}/edit', [TaskController::class, 'edit']);
Route::put('/tasks/{id}', [TaskController::class, 'update']);

Route::delete('/tasks/{id}', [TaskController::class, 'destroy'])->name('tasks.destroy');
