<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Task;

class TaskController extends Controller
{
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    public function createTask(){
        return view('tasks.create');
    }
    public function saveTask(Request $request){
        if(Auth::user()){
            $task = new Task;

            $task->body = $request->input('content');
            $task->isActive = $request->has('isActive') ? 1 : 0;
            $task->user_id = (Auth::user()->id);
            $task->save();

           return redirect('/tasks')->with('success', 'Task created successfully');
        }else{
             return redirect('/login')->with('error', 'Unauthorized. Please log in.');
             }
    }
    public function showTasks(){
        $tasks = Task::all();

        return view('tasks.show')->with('tasks', $tasks);
    }

    public function edit($id){
        $task = Task::find($id);
        return view('tasks.show')->with('tasks', $tasks);
    }
    public function update(Request $request, $id){
        $task = Task::find($id);
        if(Auth::user()->id == $task->user_id){
              $request->validate([
            'body' => 'required|string',
        ]);
            $task->body = $request->input('body');
            $task->isActive = $request->has('isActive') ? 1 : 0;
            $task->save();
         return redirect('/tasks')->with('success', 'Task updated successfully');
        }else {
         return redirect('/tasks')->with('error', 'Unauthorized to update this task');
        }
    }
    public function destroy($id)
    {
        $task = Task::find($id);
        if (Auth::user()->id == $task->user_id) {
            $task->delete();
            return redirect('/tasks')->with('success', 'Task deleted successfully');
        } else {
            return redirect('/tasks')->with('error', 'Unauthorized to delete this task');
        }
    }


}
